

## Data Processing Task

First please download [the land registry dataset](http://prod.publicdata.landregistry.gov.uk.s3-website-eu-west-1.amazonaws.com/pp-2020.csv) and copy the `pp-2020.csv` file to the `resources` directory.

In the project directory, you can use docker:
```
$ docker build -t data-processing-task .
$ docker run -it --rm --name my-running-app data-processing-task
```
and see the result in the console:
```
    area_name  market_val
0  Wandsworth   756758003
1   Islington   443723536

```

or run manually:
```
$ pip install -r requirements.txt
$ python main.py
```

and result exported as `output.csv` file in the project directory.

***Jupiter notebook was a better option to represent this project***
