import pandas as pd

areas_of_interest_df = pd.read_csv('./resources/areas_of_interest.csv')
land_registry_df = pd.read_csv('./resources/pp-2020.csv', header=None,
                               usecols=[1, 3, 12], names=["price", 'sector', 'area_name'])

total_transactions = {}

for row in areas_of_interest_df.iterrows():
    area_name = row[1]["area_name"]
    sector = row[1]["sector"]
    transaction = land_registry_df[(land_registry_df.area_name == area_name.upper()) & (
        land_registry_df.sector.str.startswith(sector))]["price"].sum()
    total_transactions[area_name] = total_transactions.get(
        area_name, 0) + transaction

market_value_df = pd.DataFrame(list(total_transactions.items()), columns=[
                               'area_name', 'market_val'])
market_value_df.to_csv('output.csv', index=False, header=True)
print(market_value_df)